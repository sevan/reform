#!/bin/bash

# Kick trackball into bootloader mode, and wait a bit
echo -ne 'xJTBL' > /dev/hidraw2
sleep 2

dfu-programmer atmega32u2 erase --suppress-bootloader-mem 
dfu-programmer atmega32u2 flash ./Mouse.hex --suppress-bootloader-mem 
dfu-programmer atmega32u2 start 
