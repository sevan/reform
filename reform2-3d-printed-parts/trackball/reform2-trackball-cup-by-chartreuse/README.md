This is a collection of my modified parts files for the MNT Reform.

# trackball
Trackball contains modified versions of the trackball 3d printed hardware. Currently the only version is an OpenSCAD modelled version of the original cup but modified to take 3x 2.5mm chrome steel ball bearings as a riding surface for the trackball, rather than the ball riding directly on the 3d printed part. 

To install the ball bearings I applied a small amount of super-glue to the holes for the bearings, and then dropped and lightly pressed the chrome balls in place.

It is a drop in replacement for the original part, and reuses the same screws and the same top retention ring. 

This file can be adapted to work without the ball bearings, and parameters tweaked for a better fit. I found that with my printer at least the result was very smooth without the balls with the layer height set to 0.3mm. Your results may vary. The "cup" is slightly larger internally than the original model as the intention was to have it solely touching the ball bearings. If using without the bearings the cup size probably should be reduced to 26mm. Another option could be to turn the bearing cutouts into a positive to make 3d printed bearings.

Current experiance is that it is very smooth with the ball bearings, though perhaps a little noiser. I've adjusted the contact angle of the balls down to 22 degrees by raising them up higher which significantly improved the experiance. It overall feels more responsive and can no longer climb the bearings, reducing play. 

Perhaps a smoother ball could make the experiance slightly better, the delrin has a bit of a texture to it which is likely where some of the noise and friction is coming from. A heavier ball would also improve the experiance.
