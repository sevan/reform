# MNT Reform Keyboard 4.0 Firmware

## Building

You have to pass 3 variables to `build.sh`:

- `-DKBD_MODE` can be `KBD_MODE_LAPTOP` or `KBD_MODE_STANDALONE`
- `-DKBD_VARIANT` can be `KBD_VARIANT_US` or `KBD_VARIANT_INTL` (for 102nd key instead of DEL next to left shift)
- `-DKBD_HID_FW_REV` is a string that is displayed as the revision on the OLED system status screen

Example:

```bash
./build.sh -DKBD_MODE=KBD_MODE_LAPTOP -DKBD_VARIANT=KBD_VARIANT_US -DKBD_HID_FW_REV=\"debug\"
```

