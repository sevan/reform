/*
  azoteq.c -- Azoteq IQS550 mappings
  Copyright 2023 Valtteri Koskivuori <vkoskiv@gmail.com>
  Copyright 2024 Lukas F. Hartmann / MNT Research GmbH <lukas@mntre.com>
  License: GPLv3
*/

#include "azoteq.h"

#include "pico/stdlib.h"
#include "hardware/i2c.h"

int read_azoteq_data(struct azoteq_data* data, size_t sz) {
  unsigned char buf[] = {0x00, 0x00, 0x00, 0x00};
  i2c_write_blocking(i2c0, ADDR_SENSOR_TP, buf, 2, true); // nostop

  int res = i2c_read_blocking_until(i2c0, ADDR_SENSOR_TP, (uint8_t*)data, sz, false, make_timeout_time_ms(100));

  // end cycle
  buf[0] = 0xee;
  buf[1] = 0xee;
  buf[2] = 0xff;
  i2c_write_blocking(i2c0, ADDR_SENSOR_TP, buf, 3, false);

  // we have to swap byte order for 16-bit quantities
  for (int i = 0; i < 5; ++i) {
    data->fingers[i].abs_x = __builtin_bswap16(data->fingers[i].abs_x);
    data->fingers[i].abs_y = __builtin_bswap16(data->fingers[i].abs_y);
  }
  data->relative_x = (int16_t)__builtin_bswap16((uint16_t)data->relative_x);
  data->relative_y = (int16_t)__builtin_bswap16((uint16_t)data->relative_y);
  
  return res;
}

