/*
             LUFA Library
     Copyright (C) Dean Camera, 2018.

  dean [at] fourwalledcubicle [dot] com
           www.lufa-lib.org
*/

/*
  Copyright 2018  Dean Camera (dean [at] fourwalledcubicle [dot] com)

  Permission to use, copy, modify, distribute, and sell this
  software and its documentation for any purpose is hereby granted
  without fee, provided that the above copyright notice appear in
  all copies and that both that the copyright notice and this
  permission notice and warranty disclaimer appear in supporting
  documentation, and that the name of the author not be used in
  advertising or publicity pertaining to distribution of the
  software without specific, written prior permission.

  The author disclaims all warranties with regard to this
  software, including all implied warranties of merchantability
  and fitness.  In no event shall the author be liable for any
  special, indirect or consequential damages or any damages
  whatsoever resulting from loss of use, data or profits, whether
  in an action of contract, negligence or other tortious action,
  arising out of or in connection with the use or performance of
  this software.
*/

#include <avr/pgmspace.h>
#include <avr/io.h>
#include <stdlib.h>

#include "i2cmaster/i2cmaster.h"
#include "azoteq.h"

#include "Mouse.h"

/** Buffer to hold the previously generated Mouse HID report, for comparison purposes inside the HID class driver. */
static uint8_t PrevMouseHIDReportBuffer[sizeof(USB_WheelMouseReport_Data_t)];

/** LUFA HID Class driver interface configuration and state information. This structure is
 *  passed to all HID Class driver functions, so that multiple instances of the same class
 *  within a device can be differentiated from one another.
 */
USB_ClassInfo_HID_Device_t Mouse_HID_Interface =
	{
		.Config =
			{
				.InterfaceNumber              = INTERFACE_ID_Mouse,
				.ReportINEndpoint             =
					{
						.Address              = MOUSE_EPADDR,
						.Size                 = MOUSE_EPSIZE,
						.Banks                = 1,
					},
				.PrevReportINBuffer           = PrevMouseHIDReportBuffer,
				.PrevReportINBufferSize       = sizeof(PrevMouseHIDReportBuffer),
			},
	};

uint8_t twi_write_reg[1];
uint8_t twi_write_buf[10];
uint8_t twi_read_buf[10];

void SetupHardware(void)
{
  /* Disable watchdog if enabled by bootloader/fuses */
  MCUSR &= ~(1 << WDRF);
  wdt_disable();

  // Disable clock division
  // this should yield 8Mhz with internal osc
  clock_prescale_set(clock_div_1);

  DDRD = 0b00000000;
  DDRB = 0b00000100;
  DDRC = 0b00000000;

  i2c_init();
  USB_Init();

  Delay_MS(100);

  i2c_start_wait(ADDR_SENSOR|I2C_WRITE);
  i2c_write(0x04);
  i2c_write(0x32);
  i2c_write(2); // reset
  i2c_stop();

  i2c_start_wait(ADDR_SENSOR|I2C_WRITE);
  i2c_write(0x04);
  i2c_write(0x32);
  i2c_write(0); // reset
  i2c_stop();

  // Disable idle mode timeout, so we never enter LP1
  i2c_start_wait(ADDR_SENSOR|I2C_WRITE);
  i2c_write(0x05);
  i2c_write(0x86);
  i2c_write(0xff); // Idle mode, 255s = never timeout
  i2c_stop();

  Delay_MS(100);
}

/** Event handler for the library USB Connection event. */
void EVENT_USB_Device_Connect(void)
{
}

/** Event handler for the library USB Disconnection event. */
void EVENT_USB_Device_Disconnect(void)
{
}

/** Event handler for the library USB Configuration Changed event. */
void EVENT_USB_Device_ConfigurationChanged(void)
{
  bool ConfigSuccess = true;

  ConfigSuccess &= HID_Device_ConfigureEndpoints(&Mouse_HID_Interface);

  USB_Device_EnableSOFEvents();
}

/** Event handler for the library USB Control Request reception event. */
void EVENT_USB_Device_ControlRequest(void)
{
  HID_Device_ProcessControlRequest(&Mouse_HID_Interface);
}

/** Event handler for the USB device Start Of Frame event. */
void EVENT_USB_Device_StartOfFrame(void)
{
  HID_Device_MillisecondElapsed(&Mouse_HID_Interface);
}

float dx = 0, dy = 0;
int16_t lastx = 0, lasty = 0;
int16_t lastx2 = 0, lasty2 = 0;
unsigned int cycle = 0;
int wheeling = 0;
int touched_time = 0;
int last_num_fingers = 0;
int start_num_fingers = 0;
int report_lift = 0;
bool g_want_bootloader = false;

#define PRESS_TIME 6
#define MOTION_CLIP 127

/** HID class driver callback function for the creation of HID reports to the host.
 *
 *  \param[in]     HIDInterfaceInfo  Pointer to the HID class interface configuration structure being referenced
 *  \param[in,out] ReportID    Report ID requested by the host if non-zero, otherwise callback should set to the generated report ID
 *  \param[in]     ReportType  Type of the report to create, either HID_REPORT_ITEM_In or HID_REPORT_ITEM_Feature
 *  \param[out]    ReportData  Pointer to a buffer where the created report should be stored
 *  \param[out]    ReportSize  Number of bytes written in the report (or zero if no report is to be sent)
 *
 *  \return Boolean \c true to force the sending of the report, \c false to let the library determine if it needs to be sent
 */
bool CALLBACK_HID_Device_CreateHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                         uint8_t* const ReportID,
                                         const uint8_t ReportType,
                                         void* ReportData,
                                         uint16_t* const ReportSize)
{
  if (ReportType==HID_REPORT_ITEM_Feature) {
    if (*ReportID==0) {
      *ReportID=2;
    }

    if (*ReportID==2) {
      USB_WheelMouseFeatureReport_Data_t* FeatureReport = (USB_WheelMouseFeatureReport_Data_t*)ReportData;
      FeatureReport->Multiplier = 2;
      *ReportSize = sizeof(USB_WheelMouseFeatureReport_Data_t);
      return true;
    } else {
      return false;
    }
  }

  if (*ReportID==0) {
    *ReportID=1;
  }

  if (*ReportID!=1) {
    return false;
  }

  USB_WheelMouseReport_Data_t* MouseReport = (USB_WheelMouseReport_Data_t*)ReportData;

  // note: look for IQS550

  MouseReport->Button = 0;
  MouseReport->Pan = 0;
  MouseReport->Wheel = 0;
  MouseReport->X = 0;
  MouseReport->Y = 0;

  // sensor ready?
  uint8_t rdy = PINB&(1<<5);
  if (!rdy) {
    if (report_lift) {
      // we still have to end a short click event
      report_lift = 0;
      *ReportSize = sizeof(USB_WheelMouseReport_Data_t);
      return true;
    }
    return false;
  }

  struct azoteq_data sensor = { 0 };
  read_azoteq_data(&sensor);

  // touched?
  if (sensor.num_fingers) {
    touched_time++;
    wheeling = 0;

    if (sensor.num_fingers != last_num_fingers) {
      lastx2 = 0;
      lasty2 = 0;
      lastx = 0;
      lasty = 0;
    }

    if (start_num_fingers < sensor.num_fingers) {
      start_num_fingers = sensor.num_fingers;
    }

    if (start_num_fingers == 2) {
      // wheel
      wheeling = 1;
    } else if (start_num_fingers == 3) {
      MouseReport->Button |= 1;
    } else if (start_num_fingers == 4) {
      // right mouse button
      //MouseReport->Button |= 2;
    }

    if (sensor.num_fingers >= 2) {
      dx = lastx && lastx2 ? ((float)sensor.fingers[1].abs_x + (float)sensor.fingers[0].abs_x) / 2.0f - ((float)lastx2 + (float)lastx) / 2.0f : 0.0f;
      dy = lasty && lasty2 ? ((float)sensor.fingers[1].abs_y + (float)sensor.fingers[0].abs_y) / 2.0f - ((float)lasty2 + (float)lasty) / 2.0f : 0.0f;
    } else {
      dx = (float)sensor.relative_x;
      dy = (float)sensor.relative_y;
    }

    if (wheeling) {
      // horizontal and vertical scrolling
      MouseReport->Pan = dx / 4.0f;
      MouseReport->Wheel = -dy / 4.0f;
    } else {
      // normal movement
      MouseReport->X = dx;
      MouseReport->Y = dy;
    }

    lastx = sensor.fingers[0].abs_x;
    lasty = sensor.fingers[0].abs_y;
    if (sensor.num_fingers > 1) {
      lastx2 = sensor.fingers[1].abs_x;
      lasty2 = sensor.fingers[1].abs_y;
    }
  } else {
    // no (more) touches

    if (start_num_fingers == 1 && touched_time > 0 && touched_time <= PRESS_TIME) {
      // tapped for a short time with 1 finger? left click
      MouseReport->Button |= 1;
    } else if (start_num_fingers == 2 && touched_time > 0 && touched_time <= PRESS_TIME) {
      // tapped for a short time with 2 fingers? right click
      MouseReport->Button |= 2;
    } else if (start_num_fingers == 3 && touched_time > 0 && touched_time <= PRESS_TIME) {
      // tapped for a short time with 3 fingers? middle click
      MouseReport->Button |= 4;
    }
    touched_time = 0;
    start_num_fingers = 0;
    report_lift = 1;
    dx = 0;
    dy = 0;
    lastx = 0;
    lasty = 0;
    lastx2 = 0;
    lasty2 = 0;
  }

  last_num_fingers = sensor.num_fingers;

  *ReportSize = sizeof(USB_WheelMouseReport_Data_t);

  return true;
}

#define BOOTLOADER_START_ADDRESS ((0x8000 - 0x1000) >> 1)
void jump_to_bootloader(void) {
  ((void (*)(void))BOOTLOADER_START_ADDRESS)();
}

#define cmd(_s) (*(uint32_t *)(_s))
#define CMD_JUMP_TO_BOOTLOADER cmd("JTBL")
/** HID class driver callback function for the processing of HID reports from the host.
 *
 *  \param[in] HIDInterfaceInfo  Pointer to the HID class interface configuration structure being referenced
 *  \param[in] ReportID    Report ID of the received report from the host
 *  \param[in] ReportType  The type of report that the host has sent, either HID_REPORT_ITEM_Out or HID_REPORT_ITEM_Feature
 *  \param[in] ReportData  Pointer to a buffer where the received report has been stored
 *  \param[in] ReportSize  Size in bytes of the received HID report
 */
void CALLBACK_HID_Device_ProcessHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                          const uint8_t ReportID,
                                          const uint8_t ReportType,
                                          const void* ReportData,
                                          const uint16_t ReportSize)
{
  // Unused (but mandatory for the HID class driver) in this demo, since there are no Host->Device reports
  // if (ReportSize < 4) return;
  const uint32_t command = *(uint32_t *)ReportData;
  if (command == CMD_JUMP_TO_BOOTLOADER) {
    g_want_bootloader = true;
  }
}


int main(void)
{
  SetupHardware();
  GlobalInterruptEnable();

  for (;;)
  {
    HID_Device_USBTask(&Mouse_HID_Interface);
    USB_USBTask();
    if (g_want_bootloader) jump_to_bootloader();
  }
}
