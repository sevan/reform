# Date: 2023-05-31
# Kernel: Linux 6.4.0-rc1+
# Processor: Amlogic A311D

minute@reform:~$ 7z b

7-Zip [64] 16.02 : Copyright (c) 1999-2016 Igor Pavlov : 2016-05-21
p7zip Version 16.02 (locale=en_US.UTF-8,Utf16=on,HugeFiles=on,64 bits,6 CPUs LE)

LE
CPU Freq: 64000000 - - - - 256000000 - - -

RAM size:    3782 MB,  # CPU hardware threads:   6
RAM usage:   1323 MB,  # Benchmark threads:      6

                       Compressing  |                  Decompressing
Dict     Speed Usage    R/U Rating  |      Speed Usage    R/U Rating
         KiB/s     %   MIPS   MIPS  |      KiB/s     %   MIPS   MIPS

22:       7009   555   1228   6819  |     127871   502   2172  10905
23:       6840   560   1244   6969  |     124767   500   2158  10796
24:       6756   564   1288   7264  |     122278   501   2144  10733
25:       6687   567   1347   7635  |     120500   502   2136  10724
----------------------------------  | ------------------------------
Avr:             561   1277   7172  |              501   2153  10789
Tot:             531   1715   8981
